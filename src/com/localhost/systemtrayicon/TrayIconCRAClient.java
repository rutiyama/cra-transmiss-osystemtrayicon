package com.localhost.systemtrayicon;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.localhost.systemtrayicon.eventos.TrayIconClickEvent;

public class TrayIconCRAClient {
	
	private PopupMenu 	popup;
	private TrayIcon 	trayIcon;
	private SystemTray 	tray;

	public TrayIconCRAClient() {
	    try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
	}
	
    private void createComponents(){
    	 popup   = new PopupMenu();
         trayIcon =  new TrayIcon(createImage("/images/java.png", "CRA-Transmissão Client"));
         tray = SystemTray.getSystemTray();
         trayIcon.setImageAutoSize(true);
    }
    
    private void createAndShowGUI() {
        //Check the SystemTray support
        if (!SystemTray.isSupported()) {
            System.out.println("SystemTray não é suportado!");
            return;
        }
        
        createComponents();
        // Create a popup menu components
        MenuItem exitItem = new MenuItem("Exit");
        MenuItem infoItem = new MenuItem("Preferências");
        
        //Add components to popup menu
        popup.add(infoItem);
        popup.addSeparator();
        popup.add(exitItem);
        
        trayIcon.setPopupMenu(popup);
        
        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            return;
        }
        
        trayIcon.addActionListener(new TrayIconClickEvent());
        exitItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tray.remove(trayIcon);
                System.exit(0);
            }
        });
    }
    
    //Obtain the image URL
    protected Image createImage(String path, String description) {
        URL imageURL = TrayIconCRAClient.class.getResource(path);
        
        if (imageURL == null) {
            System.err.println("Resource not found: " + path);
            return null;
        } else {
            return (new ImageIcon(imageURL, description)).getImage();
        }
    }
}
